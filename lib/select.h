/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   select.h                                         .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: husahuc <husahuc@student.42.fr>            +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/02/23 13:45:04 by husahuc      #+#   ##    ##    #+#       */
/*   Updated: 2019/03/09 15:13:55 by husahuc     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#ifndef SELECT_H
# define SELECT_H

# include <termios.h>
# include <unistd.h>
# include <curses.h>
# include <term.h>
# include <stdlib.h>
# include <sys/ioctl.h>

# include "../libft/libft.h"

typedef struct	s_item
{
	char			*name;
	int				select: 1;
	int				x;
	int				y;
	struct s_item	*next;
}				t_item;

typedef struct	s_global
{
	t_item	*start;
	int		pos;
	int		len_max;
	char	*cap[10];
}				t_global;

/*
** boucle.c
*/

int				list_search_letter(t_item *list, t_global **global, int letter);
void			move_key(t_global **global, int key, int *infinite);
void			print_item(t_global **global, int axe[2], t_item *list,
				struct winsize win);
void			print_win(t_global **global);
int				boucle(t_global *global);

/*
** del_item_list.c
*/

void			del_list_item(t_item **list, int item);
void			del_one_item(t_item **item);
void			del_all_list_item(t_item **item);

/*
** init_list.c
*/

t_item			*init_single_item(char *name, int *len_max);
t_item			*init_list_item(char *argv[], int *len_max);

/*
** item_list.c
*/

void			put_select(t_item *list, int item);
void			put_select_all(t_item *list, int type);
int				len_list_item(t_item *list);
int				print_list_select(t_item *list);
int				len_max_item(t_item *list);

/*
** main.c
*/

int				ft_error(int error);
int				attr(void);
void			signal_handler(void);
t_global		*init_global(char *argv[]);
int				main(int argc, char *argv[]);

/*
** signal.c
*/

void			good_order(void);
void			signal_handler_resize(int sig);
void			signal_handler_quit(int sig);
void			signal_handler_sleep(int sig);

/*
** define for key
*/

# define KEY_LEFT_S 4414235
# define KEY_RIGHT_S 4479771
# define KEY_UP_S 4283163
# define KEY_DOWN_S 4348699
# define KEY_SPACE_S 32
# define KEY_ENTER_S 10
# define KEY_SUPP_S 127
# define KEY_DELETE_S 2117294875
# define KEY_CTRL_A_S 1
# define KEY_CTRL_R_S 18
# define KEY_STAR_S 42
# define KEY_A_S 97
# define KEY_Z_S 122
# define KEY_MAJ_A_S 65
# define KEY_MAJ_Z_S 90

#endif
