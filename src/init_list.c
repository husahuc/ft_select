/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   init_list.c                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: husahuc <husahuc@student.42.fr>            +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/03/07 15:29:35 by husahuc      #+#   ##    ##    #+#       */
/*   Updated: 2019/03/07 15:29:43 by husahuc     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "../lib/select.h"

t_item	*init_single_item(char *name, int *len_max)
{
	t_item	*item;

	if (!(item = malloc(sizeof(t_item))))
		return (NULL);
	item->name = ft_strdup(name);
	if (ft_strlen(item->name) > *len_max)
		*len_max = ft_strlen(item->name);
	item->select = 0;
	item->x = 0;
	item->x = 0;
	item->next = NULL;
	return (item);
}

t_item	*init_list_item(char *argv[], int *len_max)
{
	int		i;
	t_item	*start;
	t_item	*next;
	char	**env_char;

	if (argv == NULL || argv[0] == NULL)
		return (NULL);
	if ((start = init_single_item(argv[0], len_max)) == NULL)
		return (NULL);
	next = start;
	i = 1;
	while (argv[i] != NULL)
	{
		if ((next->next = init_single_item(argv[i], len_max)) == NULL)
			return (NULL);
		next = next->next;
		i++;
	}
	return (start);
}
