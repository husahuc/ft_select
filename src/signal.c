/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   signal.c                                         .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: husahuc <husahuc@student.42.fr>            +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/03/02 10:07:35 by husahuc      #+#   ##    ##    #+#       */
/*   Updated: 2019/03/09 16:21:39 by husahuc     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "../lib/select.h"

void	good_order(void)
{
	struct termios new;

	tcgetattr(0, &new);
	new.c_lflag |= (ECHO | ICANON);
	tcsetattr(0, TCSAFLUSH, &new);
	ft_putstr_fd(tgetstr("ve", NULL), 2);
}

void	signal_handler_resize(int sig)
{
	t_global *global;

	if ((global = init_global(NULL)) == NULL)
		exit(0);
	print_win(&global);
}

void	signal_handler_quit(int sig)
{
	ft_putstr_fd(tgetstr("cl", NULL), 2);
	ft_putstr_fd(tgetstr("ve", NULL), 2);
	exit(0);
}

void	signal_handler_sleep(int sig)
{
	struct termios test;

	if (sig == SIGCONT)
	{
		signal(SIGTSTP, signal_handler_sleep);
		if (attr() == -1)
			exit(0);
		signal_handler_resize(0);
	}
	else if (sig == SIGTSTP)
	{
		tcgetattr(0, &test);
		signal(SIGTSTP, SIG_DFL);
		ioctl(2, TIOCSTI, &(test.c_cc[VSUSP]));
		good_order();
	}
}
