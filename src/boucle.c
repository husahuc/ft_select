/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   boucle.c                                         .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: husahuc <husahuc@student.42.fr>            +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/03/04 10:22:15 by husahuc      #+#   ##    ##    #+#       */
/*   Updated: 2019/03/11 09:51:50 by husahuc     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "../lib/select.h"

int			list_search_letter(t_item *list, t_global **global, int letter)
{
	t_item	*tmp;
	int		i;
	int		j;

	i = 0;
	while (list)
	{
		if (list->name[0] == letter)
		{
			tmp = list;
			j = 0;
			while (i + j < (*global)->pos + 1)
			{
				tmp = tmp->next;
				if (tmp == NULL || tmp->name[0] != letter)
					return (i);
				j++;
			}
			return (i + j);
		}
		list = list->next;
		i++;
	}
	return (-1);
}

void		move_key(t_global **global, int key, int *infinite)
{
	if ((key == KEY_LEFT_S || key == KEY_UP_S))
		(*global)->pos++;
	else if ((key == KEY_RIGHT_S || key == KEY_DOWN_S))
		(*global)->pos--;
	else if (key == KEY_SPACE_S)
	{
		put_select((*global)->start, (*global)->pos);
		(*global)->pos++;
	}
	else if (key == KEY_ENTER_S)
		*infinite = 0;
	else if (key == KEY_SUPP_S || key == KEY_DELETE_S)
		del_list_item(&(*global)->start, (*global)->pos);
	else if (key == KEY_CTRL_A_S || key == KEY_STAR_S)
		put_select_all((*global)->start, 1);
	else if (key == KEY_CTRL_R_S)
		put_select_all((*global)->start, 0);
	else if ((key >= KEY_A_S && key <= KEY_Z_S) ||
		(key >= KEY_MAJ_A_S && key <= KEY_MAJ_Z_S))
		if (list_search_letter((*global)->start, global, key) != -1)
			(*global)->pos = list_search_letter((*global)->start, global, key);
	if ((*global)->pos >= len_list_item((*global)->start))
		(*global)->pos = 0;
	if ((*global)->pos == -1)
		(*global)->pos = len_list_item((*global)->start) - 1;
}

void		print_item(t_global **global, int axe[2], t_item *list,
			struct winsize win)
{
	if ((axe[0] + 1) * ((*global)->len_max + 1) > win.ws_col)
	{
		axe[0] = 0;
		axe[1]++;
	}
	ft_putstr_fd(tgoto((*global)->cap[4],
		axe[0] * ((*global)->len_max + 1), axe[1]), 2);
	if ((*global)->pos == axe[2])
		ft_putstr_fd((*global)->cap[1], 2);
	if (list->select == -1)
		ft_putstr_fd((*global)->cap[2], 2);
	ft_putstr_fd(list->name, 2);
	ft_putstr_fd((*global)->cap[5], 2);
	axe[0]++;
	axe[2]++;
}

void		print_win(t_global **global)
{
	struct winsize	win;
	int				axe[3];
	t_item			*list;

	axe[0] = 0;
	axe[1] = 0;
	axe[2] = 0;
	list = (*global)->start;
	ft_putstr_fd((*global)->cap[0], 2);
	ioctl(0, TIOCGWINSZ, &win);
	ft_putstr_fd((*global)->cap[6], 2);
	if (win.ws_row * (win.ws_col / ((*global)->len_max + 1))
		< len_list_item((*global)->start))
	{
		ft_putstr("please resize");
		return ;
	}
	while (list)
	{
		print_item(global, axe, list, win);
		list = list->next;
	}
}

int			boucle(t_global *global)
{
	int			infinite;
	long		key;

	infinite = 1;
	while (infinite)
	{
		key = 0;
		print_win(&global);
		read(0, &key, sizeof(key));
		if (key == KEY_LEFT_S || key == KEY_RIGHT_S || key == KEY_ENTER_S
			|| key == KEY_UP_S || key == KEY_DOWN_S || key == KEY_SPACE_S
			|| key == KEY_SUPP_S || key == KEY_DELETE_S || key == KEY_STAR_S
			|| key == KEY_CTRL_A_S || key == KEY_CTRL_R_S
			|| (key >= KEY_A_S && key <= KEY_Z_S)
			|| (key >= KEY_MAJ_A_S && key <= KEY_MAJ_Z_S))
			move_key(&global, key, &infinite);
		if (key == 27 || len_list_item(global->start) < 1)
			return (0);
		global->len_max = len_max_item(global->start);
	}
	ft_putstr_fd(tgoto(global->cap[4], 0, 0), 2);
	ft_putstr_fd(global->cap[0], 2);
	print_list_select(global->start);
	return (1);
}
