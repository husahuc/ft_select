/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   main.c                                           .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: husahuc <husahuc@student.42.fr>            +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/02/23 13:42:38 by husahuc      #+#   ##    ##    #+#       */
/*   Updated: 2019/03/11 09:51:51 by husahuc     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "../lib/select.h"

int			ft_error(int error)
{
	if (error == 0)
		ft_putendl("usage: ft_select [...]\n\narrow to move \
			\nspace to select\nctrl a or * to select all \
			\nctrl r to unselect all \
			\nany key to search the first letter \
			\nctrl z to suspend");
	else if (error == 1)
		ft_putendl("no TERM env");
	else if (error == 2)
		ft_putendl("malloc failed");
	else if (error == 3)
		ft_putendl("Termios failed");
	return (1);
}

int			attr(void)
{
	struct termios new;

	if (tcgetattr(0, &new) == -1)
		return (-1);
	new.c_lflag &= ~(ECHO | ICANON);
	if (tcsetattr(0, TCSAFLUSH, &new) == -1)
		return (-1);
	return (0);
}

void		signal_handler(void)
{
	signal(SIGINT, signal_handler_quit);
	signal(SIGHUP, signal_handler_quit);
	signal(SIGQUIT, signal_handler_quit);
	signal(SIGILL, signal_handler_quit);
	signal(SIGABRT, signal_handler_quit);
	signal(SIGKILL, signal_handler_quit);
	signal(SIGSEGV, signal_handler_quit);
	signal(SIGTERM, signal_handler_quit);
	signal(SIGBUS, signal_handler_quit);
	signal(SIGTSTP, signal_handler_sleep);
	signal(SIGCONT, signal_handler_sleep);
	signal(SIGWINCH, signal_handler_resize);
}

t_global	*init_global(char *argv[])
{
	int				len_max;
	static t_global	*global = NULL;

	if (argv == NULL)
	{
		global->len_max = len_max_item(global->start);
		return (global);
	}
	if (!(global = malloc(sizeof(t_global))))
		return (NULL);
	global->pos = 0;
	global->cap[0] = tgetstr("cl", NULL);
	global->cap[1] = tgetstr("us", NULL);
	global->cap[2] = tgetstr("mr", NULL);
	global->cap[3] = tgetstr("co", NULL);
	global->cap[4] = tgetstr("cm", NULL);
	global->cap[5] = tgetstr("me", NULL);
	global->cap[6] = tgetstr("vi", NULL);
	global->cap[7] = tgetstr("ve", NULL);
	if ((global->start = init_list_item(argv + 1, &len_max)) == NULL)
		return (NULL);
	global->len_max = len_max;
	return (global);
}

int			main(int argc, char *argv[])
{
	char		*term_type;
	t_global	*global;

	signal_handler();
	if (argc < 2)
		return (ft_error(0));
	if ((term_type = getenv("TERM")) == NULL || tgetent(NULL, term_type) <= 0)
		return (ft_error(1));
	if ((global = init_global(argv)) == NULL || attr() == -1)
		return (ft_error(2));
	ft_putstr_fd(global->cap[0], 2);
	ft_putstr_fd(global->cap[6], 2);
	if (!boucle(global))
		ft_putstr(global->cap[0]);
	del_all_list_item(&(global->start));
	good_order();
	free(global);
	return (0);
}
