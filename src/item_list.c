/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   item_list.c                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: husahuc <husahuc@student.42.fr>            +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/02/23 13:58:31 by husahuc      #+#   ##    ##    #+#       */
/*   Updated: 2019/03/09 15:13:38 by husahuc     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "../lib/select.h"

void	put_select(t_item *list, int item)
{
	while (list && item--)
	{
		list = list->next;
		if (list == NULL)
			return ;
	}
	if (list->select == 0)
		list->select = 1;
	else
		list->select = 0;
}

void	put_select_all(t_item *list, int type)
{
	while (list)
	{
		list->select = type;
		list = list->next;
	}
}

int		len_list_item(t_item *list)
{
	int i;
	int len;

	i = 0;
	while (list)
	{
		list = list->next;
		i++;
	}
	return (i);
}

int		print_list_select(t_item *list)
{
	int i;

	i = 0;
	while (list)
	{
		if (list->select == -1)
		{
			if (i > 0)
				ft_putstr(" ");
			ft_putstr(list->name);
			i++;
		}
		list = list->next;
	}
	return (1);
}

int		len_max_item(t_item *list)
{
	int len;

	len = 0;
	while (list)
	{
		if (ft_strlen(list->name) > len)
			len = ft_strlen(list->name);
		list = list->next;
	}
	return (len);
}
