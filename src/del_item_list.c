/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   del_item_list.c                                  .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: husahuc <husahuc@student.42.fr>            +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/02/23 15:37:07 by husahuc      #+#   ##    ##    #+#       */
/*   Updated: 2019/03/07 12:08:04 by husahuc     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "../lib/select.h"

void		del_list_item(t_item **list, int item)
{
	t_item	*next;
	t_item	*pres;
	t_item	*present;

	if (item == 0)
	{
		next = (*list)->next;
		del_one_item(list);
		*list = next;
		return ;
	}
	present = *list;
	while (present != NULL)
	{
		if (item == 0)
		{
			next = present->next;
			del_one_item(&present);
			pres->next = next;
			return ;
		}
		pres = present;
		present = present->next;
		item--;
	}
}

void		del_one_item(t_item **item)
{
	if (*item)
	{
		free((*item)->name);
		free(*item);
	}
}

void		del_all_list_item(t_item **item)
{
	if (item == NULL || *item == NULL)
		return ;
	if ((*item)->next)
		del_all_list_item(&(*item)->next);
	del_one_item(item);
}
