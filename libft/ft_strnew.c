/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_strnew.c                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: husahuc <husahuc@student.42.fr>            +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/10/04 10:17:36 by husahuc      #+#   ##    ##    #+#       */
/*   Updated: 2019/03/02 15:07:42 by husahuc     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"

char		*ft_strnew(size_t size)
{
	char	*ptr_new;
	size_t	i;

	if (!(ptr_new = (char*)malloc((size + 1) * sizeof(char))))
		return (NULL);
	i = 0;
	while (size-- > 0)
		ptr_new[i++] = '\0';
	ptr_new[i] = '\0';
	return (ptr_new);
}
