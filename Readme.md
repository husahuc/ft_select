# Ft_select
projet fait pour l'école 42 Lyon, consistant à faire un menu pour sélectionner des options dans une interface sur terminal.

# objectif
Le projet fait voir les Termcaps et leurs utilisations pour créer une interface terminal avec le mode non cannonique. Mais aussi les signaux, comment reprendre normalement le prompt après que le programme se soit arrêté, ou suspendu.
Faire le déplacement et la sélection avec la fleche, savoir où est positioné le curseur. Et la gestion des signaux, notamment pour reprendre le programme.
En petit plus une recherche sur le premier charactère.

# Code
Après avoir vérifié les paramètres et les variables d'environnement donné au programme, les Termcaps et les paramètre sont stocké dans une structure globale. Le terminal est passé en mode non canonique, et les signaux sont mis en place. Le programme est mis dans une boucle infinie, ou il va afficher tous les paramètres et la position du curseur. Les actions (touche du clavier) sont vérifiées, si la touche del est selectionnè, l'élément dans la liste chainée est effacé et la position du curseur est mise à jour. S'il n'y a plus d'éléments dans la liste, le terminal est restauré. Si la touche entrée, les éléments de la liste sélectionnée sont affiches et le terminal est retourné.


![ft_select](https://gitlab.com/husahuc/ft_select/raw/master/ft_select_1.png)
![ft_select](https://gitlab.com/husahuc/ft_select/raw/master/ft_select_2.png)
![ft_select](https://gitlab.com/husahuc/ft_select/raw/master/ft_select_3.png)