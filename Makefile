# **************************************************************************** #
#                                                           LE - /             #
#                                                               /              #
#    Makefile                                         .::    .:/ .      .::    #
#                                                  +:+:+   +:    +:  +:+:+     #
#    By: husahuc <husahuc@student.42.fr>            +:+   +:    +:    +:+      #
#                                                  #+#   #+    #+    #+#       #
#    Created: 2019/02/23 14:04:18 by husahuc      #+#   ##    ##    #+#        #
#    Updated: 2019/03/07 15:29:53 by husahuc     ###    #+. /#+    ###.fr      #
#                                                          /                   #
#                                                         /                    #
# **************************************************************************** #

CC = gcc
CFLAGS += -Wall -Wextra -Werror
SRC_PATH = src
SRC_NAME = main.c item_list.c del_item_list.c signal.c boucle.c init_list.c
OBJ_PATH = obj/
OBJ_NAME = $(SRC_NAME:.c=.o)
SRC = $(addprefix $(SRC_PATH)/,$(SRC_NAME))
OBJ = $(addprefix $(OBJ_PATH)/,$(OBJ_NAME))
RM = rm -f

NAME = ft_select

.PHONY: clean fclean re

all: $(NAME)

$(NAME): $(OBJ)
	@$(MAKE) -C libft
	@$(CC) $(CFLAGS) -ltermcap libft/libft.a  $(OBJ) -o $@
	@printf "\n\033[0;32m[OK]\033[0m\033[0;33m Compiling minishell:\033[0m %s" $(NAME)

$(OBJ_PATH)%.o: $(SRC_PATH)%.c lib/select.h
	@$(CC) -c $< -o $@
	@printf "\033[0;32m[OK]\033[0m %s " $<

clean:
	@make clean -C libft
	@$(RM) $(OBJ)

fclean: clean
	@make fclean -C libft
	@$(RM) $(NAME)
	@printf "\033[0;32m[OK]\033[0m \033[0;33msupress:\033[0m %s\n" $(NAME)
	@echo "\033[0;32m[OK]\033[0m \033[0;33msupress:\033[0m" $(OBJ)

re: clean all
